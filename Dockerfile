FROM node:10-alpine AS builder
WORKDIR /app
COPY . ./
RUN yarn install --production && yarn add --dev webpack webpack-cli && yarn run build-prod

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/dist .
ENTRYPOINT ["nginx", "-g", "daemon off;"]