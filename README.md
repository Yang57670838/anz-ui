### `run locally`

yarn install<br />
yarn start<br />
http://localhost:9000/ <br />
please login with account number '123456789' and password 'password'

### `type check`

yarn run type-check

### `unit test`

yarn run test <br />
yarn run test-coverage

### `run with nginx in production env`

docker build -t yang/anz-ui:v1 . <br />
docker run --rm -it --name anz-ui -p 8080:80 yang/anz-ui:v1 <br />
visit 'http://localhost:8080/'

### `features`

1, a login page which mocks the login  process with a basic inline validation and server validation, please login with account number '123456789' and password 'password'. The mock credentials are located at 'src/mocks/account' <br />

2, an account list page will mock fetching list of accounts belong to the login credential and  display in a data-grid table, if user click any account row, it will redirect to the transaction list page<br />

3, mobile friendly: it will access current screen size by Material UI's media query hook, and change the styles of both header menu and list of products through flexbox underneath.. <br />

4, the transaction list page will mock fetching all transaction histories from server and display them in the data-grid table, the mock data is located at 'src/mocks/transaction' <br />

5, nginx as the dockerized virtual hosting for the static files in production env, enabled http cache and http compression <br />

6, enable production optimization such as bundle split, lazy loading..

### `UI`

**Login Page**

![alt text](/loginpage.png)

**Account List Page**

![alt text](/accountlist.png)

**Transaction History Page**

![alt text](</transactionlist.png>)
