const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const rules = [
    {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader'
        },
    },
    {
        test: /\.m?js/,
        resolve: {
            fullySpecified: false
        }
    },
    {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
    },
    {
        test: /\.(png|svg|jpg|jpeg|gif|ico|json)$/,
        exclude: /node_modules/,
        use: ['file-loader?name=[name].[ext]']
    }
]

module.exports = configDirs => {

    return {
        entry: {
            main: [configDirs.APP_DIR + '/index']
        },
        output: {
            path: configDirs.BUILD_DIR,
            filename: '[name].[contenthash].js',
            publicPath: '/'
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js'],
            alias: {
                components: configDirs.APP_DIR + '/components',
                constants: configDirs.APP_DIR + '/constants',
                mocks: configDirs.APP_DIR + '/mocks',
                store: configDirs.APP_DIR + '/store',
                types: configDirs.APP_DIR + '/types',
                utility: configDirs.APP_DIR + '/utility'
            }
        },
        module: { rules },
        plugins: [
            new HtmlWebpackPlugin({
                inject: true,
                template: configDirs.PUBLIC_DIR + '/index.html',
                favicon: configDirs.PUBLIC_DIR+ '/favicon.ico',
                minify: {
                    removeComments: true,
                    collapseWhitespace: true
                }
            }),
            new CleanWebpackPlugin()
        ]
    }
};