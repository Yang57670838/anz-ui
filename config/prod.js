const { merge } = require('webpack-merge')
const commonConfig = require('./common')

const prodConfig = {
    optimization: {
        splitChunks: {
            chunks: "all",
        },
        runtimeChunk: {
            name: "runtime",
        }
    },
    performance: {
        hints: "error", // make sure ci will fail for oversized
        maxAssetSize: 1000 * 1024, // 1MB
        maxEntrypointSize: 1000 * 1024, // 1MB
    }
};

module.exports = (configDirs) => {
    return merge(commonConfig(configDirs), prodConfig);
}
  