module.exports = {
    preset: 'ts-jest',
    snapshotSerializers: ['enzyme-to-json/serializer'],
    testEnvironment: 'node',
    clearMocks: true,
    transform: {
        '^.+\\.tsx?$': 'ts-jest'
    },
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    moduleNameMapper: {
        "^components/(.*)": "<rootDir>/src/components/$1",
        "^store/(.*)": "<rootDir>/src/store/$1",
        "^constants/(.*)": "<rootDir>/src/constants/$1",
        "^mocks/(.*)": "<rootDir>/src/mocks/$1",
        "^types/(.*)": "<rootDir>/src/types/$1",
        "^utility/(.*)": "<rootDir>/src/utility/$1"
    },
    setupFiles: ['<rootDir>/setupTests.ts'],
    collectCoverageFrom: [
        "<rootDir>/src/components/**/*.{js,jsx,ts,tsx}",
        "!<rootDir>/src/**/*.d.{js,ts}"
    ],
    coverageThreshold: {
        global: {
            branches: 0,
            functions: 0,
            lines: 0,
            statements: 0,
        },
    },
    coverageDirectory: 'coverage'
};