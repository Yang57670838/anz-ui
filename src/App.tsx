import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import theme from './styles/theme'
import Header from 'components/organisms/Header'
import ProtectedRoute from 'components/pages/ProtectedRoute'

const Login = React.lazy(() => import(/* webpackChunkName: "Login" */ 'components/pages/Login'))
const Logout = React.lazy(() => import(/* webpackChunkName: "Logout" */ 'components/pages/Logout'))
const Page = React.lazy(() => import(/* webpackChunkName: "Page" */ 'components/pages/Page'))
const Transaction = React.lazy(() => import(/* webpackChunkName: "Transaction" */ 'components/pages/Transaction'))

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <React.Suspense fallback={<span>Loading...</span>}>
        <BrowserRouter>
          <Header />
          <Switch>
            <ProtectedRoute exact={true} path='/' component={Page} />
            <ProtectedRoute exact={true} path='/transaction/:accountno' component={Transaction} />
            <Route exact={true} path='/login' component={Login} />
            <Route exact={true} path='/logout' component={Logout} />
          </Switch>
        </BrowserRouter>
      </React.Suspense>
    </ThemeProvider>
  );
}

export default App;