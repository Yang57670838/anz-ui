import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { DataGrid, ColDef, SelectionChangeParams } from '@material-ui/data-grid'
import { RouteComponentProps } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom';
import { accountActions } from 'store/actions'
import { Store } from 'types/common/store'
import { AccountDetail } from 'types/account'

const useStyles = makeStyles({
    accountsTableContainer: {
        marginTop: '100px',
        width: '1200px',
        marginRight: 'auto',
        marginLeft: 'auto',

        '& .accounts-table-header': {
            backgroundColor: '#DFE7EB'
        },

        '& .accounts-table-cell': {
            cursor: 'pointer'
        },

        '& .accounts-table-number-cell': {
            cursor: 'pointer',
            color: '#57A055'
        },
    }
})

const columns: ColDef[] = [
    {
        field: 'accountNumber',
        headerName: 'Account Number',
        width: 200,
        headerClassName: 'accounts-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'accounts-table-cell'
    },
    {
        field: 'accountName',
        headerName: 'Account Name',
        sortable: false,
        width: 200,
        headerClassName: 'accounts-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'accounts-table-cell'
    },
    {
        field: 'accountType',
        headerName: 'Account Type',
        width: 200,
        headerClassName: 'accounts-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'accounts-table-cell'
    },
    {
        field: 'balanceDate',
        headerName: 'Balance Date',
        type: 'date',
        width: 200,
        headerClassName: 'accounts-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'accounts-table-cell'
    },
    {
        field: 'currency',
        headerName: 'Currency',
        headerClassName: 'accounts-table-header',
        headerAlign: 'center',
        align: 'center',
        width: 150,
        cellClassName: 'accounts-table-cell'
    },
    {
        field: 'openingAvailableBalance',
        headerName: 'Opening Available Balance',
        type: 'number',
        width: 250,
        headerClassName: 'accounts-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'accounts-table-number-cell'
    }
];

interface OwnProps extends RouteComponentProps {}

interface StateProps {
    accountsArray: AccountDetail[] | null;
    paccount: string | null;
}

interface DispatchProps {
    fetchAccounts: (paccount: string) => void;
}

type AllProps = StateProps & DispatchProps & OwnProps;

const AccountList: React.FC<AllProps> = (props) => {

    const classes = useStyles()

    const [visitingTransactionID, setVisitingTransactionID] = useState<number | null>(null);

    useEffect(() => {
        if (props.paccount) {
            props.fetchAccounts(props.paccount)
        }
    }, [props.paccount])

    useEffect(() => {
        if (visitingTransactionID) {
          props.history.push(`/transaction/${visitingTransactionID}`);
        }
      }, [visitingTransactionID, props.history]);

    const selectRowHandler = (newSelection: SelectionChangeParams) => {
        if (newSelection.rows.length>0) {
            setVisitingTransactionID(newSelection.rows[0].id as number)
            
        }
    }

    return (
        <div id='data-table-accountlist' className={classes.accountsTableContainer}>
            {
                props.accountsArray && props.accountsArray.length > 0 && (
                    <>
                        {/* autoHeight is only recommended for small data set */}
                        <DataGrid
                            rows={props.accountsArray}
                            columns={columns}
                            pageSize={5}
                            rowsPerPageOptions={[5, 10, 20]}
                            autoHeight
                            onSelectionChange={selectRowHandler}
                        />
                    </>
                )

            }

        </div>
    );
}

const mapStateToProps: (state: Store) => StateProps = state => ({
    accountsArray: state.account.accounts && Object.keys(state.account.accounts).map(id => {
        return (state.account.accounts as any)[id]
    }),
    paccount: state.login.paccount
});

const mapDispatchToProps: DispatchProps = {
    fetchAccounts: accountActions.fetchAccountsAction
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AccountList));