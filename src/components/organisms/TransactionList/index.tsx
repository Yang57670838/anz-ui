import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { DataGrid, ColDef, RowData, ValueFormatterParams } from '@material-ui/data-grid'
import { RouteComponentProps } from 'react-router'
import { withRouter } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles'
import { Store } from 'types/common/store'
import { AccountDetail } from 'types/account'
import { Transaction } from 'types/transaction'
import { transactionActions } from 'store/actions'

const useStyles = makeStyles({
    accountsTableContainer: {
        marginTop: '100px',
        width: '1600px',
        marginRight: 'auto',
        marginLeft: 'auto',

        '& .transactions-table-header': {
            backgroundColor: '#DFE7EB'
        },
        '& .transactions-table-cell': {
            cursor: 'pointer'
        },
        '& .transactions-table-number-cell': {
            cursor: 'pointer',
            color: '#57A055'
        },
    }
})

const columns: ColDef[] = [
    {
        field: 'accountNumber',
        headerName: 'Account Number',
        sortable: false,
        width: 200,
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'transactions-table-cell'
    },
    {
        field: 'accountName',
        headerName: 'Account Name',
        sortable: false,
        width: 200,
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'transactions-table-cell'
    },
    {
        field: 'valueDate',
        headerName: 'Value Date',
        type: 'date',
        width: 200,
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'transactions-table-cell'
    },
    {
        field: 'currency',
        headerName: 'Currency',
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        width: 150,
        cellClassName: 'transactions-table-cell'
    },
    {
        field: 'debitAmount',
        headerName: 'Debit Amount',
        type: 'number',
        width: 200,
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'transactions-table-number-cell',
    },
    {
        field: 'creditAmount',
        headerName: 'Credit Amount',
        type: 'number',
        width: 200,
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        cellClassName: 'transactions-table-number-cell'
    },
    {
        field: 'debitOrCredit',
        headerName: 'Debit/Credit',
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        width: 150,
        cellClassName: 'transactions-table-cell'
    },
    {
        field: 'transactionNarrative',
        headerName: 'Transaction Narrative',
        headerClassName: 'transactions-table-header',
        headerAlign: 'center',
        align: 'center',
        width: 300,
        cellClassName: 'transactions-table-cell'
    },
];

interface OwnProps extends RouteComponentProps<{ accountno: string }> { }

interface StateProps {
    transactions: Transaction[];
}

interface DispatchProps {
    fetchTransactions: (accountID: number) => void;
}

type AllProps = StateProps & DispatchProps & OwnProps;

const TransactionList: React.FC<AllProps> = (props) => {

    const classes = useStyles()

    useEffect(() => {
        props.fetchTransactions(Number(props.match.params.accountno))
    }, [])

    return (
        <div id='data-table-accountlist' className={classes.accountsTableContainer}>
            {
                props.transactions && props.transactions.length > 0 && (
                    <>
                        {/* autoHeight is only recommended for small data set */}
                        <DataGrid
                            rows={props.transactions}
                            columns={columns}
                            pageSize={5}
                            rowsPerPageOptions={[5, 10, 20]}
                            autoHeight
                        />
                    </>
                )

            }

        </div>
    );
}

const mapStateToProps: (state: Store, ownProps: OwnProps) => StateProps = (state, ownProps) => ({
    transactions: state.transaction.transactions.map(e => {
        const accountDetail = state.account.accounts && state.account.accounts[Number(ownProps.match.params.accountno)]
        return {
            ...e,
            accountNumber: accountDetail && accountDetail.accountNumber,
            accountName: accountDetail && accountDetail.accountName,
            currency: accountDetail && accountDetail.currency
        }
    })
});

const mapDispatchToProps: DispatchProps = {
    fetchTransactions: transactionActions.fetchTransactionsAction
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TransactionList));