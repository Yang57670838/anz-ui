import React, { useState, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import InputAdornment from '@material-ui/core/InputAdornment'
import CheckIcon from '@material-ui/icons/Check'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormHelperText from '@material-ui/core/FormHelperText';
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import { green } from '@material-ui/core/colors'
import Checkbox from '@material-ui/core/Checkbox'
import ClearIcon from '@material-ui/icons/Clear'
import { isAccountNumber } from 'utility/validate'
import { Store } from 'types/common/store'
import { loginActions } from 'store/actions'

const useStyles = makeStyles({
    loginCard: {
        marginTop: '30vh'
    },
    paper: {
        padding: '50px'
    },
    button: {
        width: '100%'
    }
})

interface StateProps {
    isLogin: string | null;
    hasError: boolean;
}

interface DispatchProps {
    login: (account: string, password: string, rememberMe: boolean) => void;
}

interface OwnProps extends RouteComponentProps { }

type AllProps = StateProps & DispatchProps & OwnProps;

const Login: React.FC<AllProps> = ({ isLogin, login, hasError, history }) => {

    const [account, setAccount] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [rememberMe, setRememberMe] = useState<boolean>(false)
    const [accountErr, setAccountErr] = useState<boolean | undefined>(undefined)
    const [passwordErr, setPasswordErr] = useState<boolean | undefined>(undefined)

    const classes = useStyles()

    useEffect(() => {
        const savedAccountID = localStorage.getItem('paccount');
        if (savedAccountID) {
            setAccount(savedAccountID)
            setRememberMe(true)
        }
    }, [])

    useEffect(() => {
        if (isLogin) {
            history.push('/')
        }
    }, [isLogin])

    const inputPasswordHandler = (event: React.ChangeEvent<{ value: string }>) => {
        setPassword(event.target.value)
        if (event.target.value) {
            setPasswordErr(false)
        } else {
            setPasswordErr(true)
        }
    }

    const inputAccountHandler = (event: React.ChangeEvent<{ value: string }>) => {
        setAccount(event.target.value)
        if (isAccountNumber(event.target.value)) {
            setAccountErr(false)
        } else {
            setAccountErr(true)
        }
    }

    const rememberMeCheckedHandler = () => {
        setRememberMe(!rememberMe)
    }

    const handleLogin = () => {
        login(account, password, rememberMe)
    }

    return (
        <div className={classes.loginCard}>
            <Grid container spacing={0} justify='center' direction='row'>
                <Grid item>
                    <Paper
                        variant='elevation'
                        elevation={2}
                        className={classes.paper}
                    >
                        <Grid
                            container
                            direction='column'
                            justify='center'
                            spacing={2}
                        >
                            <Grid item>
                                <Typography variant='h5'>
                                    ANZ UI
                                </Typography>
                            </Grid>
                            <Grid item>
                                <TextField
                                    id='accountID'
                                    label='Account Number'
                                    variant='outlined'
                                    type='number'
                                    error={accountErr}
                                    helperText={accountErr ? 'Account must be 1-9 digits' : ''}
                                    value={account}
                                    onChange={inputAccountHandler}
                                    InputProps={{
                                        endAdornment: <InputAdornment position='end'>
                                            {
                                                accountErr === undefined ? <></> : accountErr ? <ClearIcon color='error' /> : <CheckIcon style={{ color: green[500] }} />
                                            }
                                        </InputAdornment>
                                    }}
                                    required
                                />
                            </Grid>
                            <Grid item>
                                <TextField
                                    id='password'
                                    label='Password'
                                    variant='outlined'
                                    type='password'
                                    error={passwordErr}
                                    value={password}
                                    onChange={inputPasswordHandler}
                                    InputProps={{
                                        endAdornment: <InputAdornment position='end'>
                                            {
                                                passwordErr === undefined ? <></> : passwordErr ? <ClearIcon color='error' /> : <CheckIcon style={{ color: green[500] }} />
                                            }
                                        </InputAdornment>
                                    }}
                                    required
                                />
                            </Grid>
                            <Grid item>
                                {
                                    hasError && (
                                        <FormHelperText error>Please make sure login credential is correct.</FormHelperText>
                                    )
                                }
                            </Grid>
                            <Grid item>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={rememberMe}
                                            onChange={rememberMeCheckedHandler}
                                            name='remembermeCheck'
                                            color='primary'
                                        />
                                    }
                                    label='Remember Account Number'
                                />
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    type='submit'
                                    className={classes.button}
                                    disabled={accountErr || !account || passwordErr || !password}
                                    onClick={handleLogin}
                                >
                                    Login
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}

const mapStateToProps: (state: Store) => StateProps = state => ({
    isLogin: state.login && state.login.token, // check if logged in already then redirect to home page
    hasError: state.login && state.login.error
});

const mapDispatchToProps: DispatchProps = {
    login: loginActions.loginAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)