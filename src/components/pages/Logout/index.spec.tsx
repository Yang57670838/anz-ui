import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { Logout, AllProps } from './index';
import getMockRouterProps from 'mocks/getRouterProps'

let wrapper: ShallowWrapper<{}>;
const mockHistoryPush = jest.fn();
const defaultProps: AllProps = {
    logout: () => { },
    history: {
        ...getMockRouterProps({}).history,
        push: mockHistoryPush
    },
    location: getMockRouterProps({}).location,
    match: getMockRouterProps({}).match
};

beforeEach(() => {
    const Component = <Logout {...defaultProps} />;
    wrapper = shallow(Component);
});

describe('<Logout />', () => {
    it('it matches the snapshot', () => {
        expect(wrapper).toMatchSnapshot();
    });

    it('Component contents should render successfully with correct contents', () => {
        expect(wrapper.find('Fragment').text()).toEqual('...loging out now');
    });
});