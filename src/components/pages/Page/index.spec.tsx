import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import Typography from '@material-ui/core/Typography'
import AccountList from '../../organisms/AccountList'

import Page from './index';

let wrapper: ShallowWrapper<{}>;

beforeEach(() => {
  const PageComponent = <Page />;
  wrapper = shallow(PageComponent);
});

describe('<Page />', () => {
  it('it matches the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('Component contents should render successfully with correct contents', () => {
    expect(wrapper.find(Typography).length).toEqual(1);
    expect(wrapper.find(Typography).text()).toEqual('Accounts');
    expect(wrapper.find(Typography).props().variant).toEqual('h2');
    expect(wrapper.find(AccountList).length).toEqual(1);
  });
});