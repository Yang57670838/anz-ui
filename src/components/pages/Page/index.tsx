import React from 'react'
import Typography from '@material-ui/core/Typography'
import AccountList from '../../organisms/AccountList'

const Page: React.FC = () => {
    return (
        <>
            <Typography variant='h2'>
                Accounts
            </Typography>
            <AccountList />
        </>
    );
}

export default Page;