import React from 'react'
import Typography from '@material-ui/core/Typography'
import TransactionList from '../../organisms/TransactionList'

const Transaction: React.FC = () => {
    return (
        <>
            <Typography variant='h2'>
                Transactions
            </Typography>
            <TransactionList />
        </>
    );
}

export default Transaction;