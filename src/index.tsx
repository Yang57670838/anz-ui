import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore, persistReducer, PersistConfig } from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import storage from 'redux-persist/lib/storage'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import App from './App'
import reducers from 'store/reducers'

const persistConfig: PersistConfig<any> = {
    key: 'anz-ui',
    storage, // localstorage
    stateReconciler: autoMergeLevel2,
    whitelist: ['login', 'account'] // persist login state only here.. so refresh will keep user logged on..
}
const persistedReducer = persistReducer(persistConfig, reducers as any)

// only use redux logger in dev env, need to change process.env in other envs
const composeEnhancers = process.env.NODE_ENV !== 'production' && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const middlewares = []
if (process.env.NODE_ENV !== 'production') {
    const { createLogger } = require('redux-logger')
    const logger = createLogger({ duration: true })
    middlewares.push(logger)
}

const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(...middlewares)))

const  persistor = persistStore(store)

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);