import { AccountDetail } from 'types/account'

export const JWTmock = {
    value: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJhbnouY29tLmF1IiwiZXhwIjoxNjM1MDc5NzM3LCJzdWIiOiIxMjM0NSIsImF1ZCI6ImFuelVJIiwianRpIjoiMTExMTEiLCJuYW1lIjoiWWFuZyBMaXUiLCJwYWNjb3VudCI6IjEyMzQ1Njc4In0.n7Ff-gucmpO4VFi3Pv5IeEIa9YTjZ7uGsgnVV4fRg-c',
    header: {
        'alg': 'HS256',
        'typ': 'JWT'
    },
    payload: {
        'iss': 'anz.com.au',
        'exp': 1635079737, // 2021-10-24
        'sub': '12345',
        'aud': 'anzUI',
        'jti': '11111',
        'name': 'Yang Liu',
        'paccount': '123456789' // assume to send request or login based on primary account number, 8 digits
    }
}

export const loginMock = {
    'paccount': '123456789',
    'password': 'password'
}

export const accountList: AccountDetail[] = [
    {
        id: 1,
        accountNumber: 585309209,
        accountName: 'SGSavings736',
        accountType: 'Savings',
        balanceDate: '08/11/2018',
        currency: 'SGD',
        openingAvailableBalance: 84327.51
    },
    {
        id: 2,
        accountNumber: 791066619,
        accountName: 'AUSavings933',
        accountType: 'Savings',
        balanceDate: '08/11/2018',
        currency: 'AUD',
        openingAvailableBalance: 88005.93
    },
    {
        id: 3,
        accountNumber: 321143048,
        accountName: 'AUCurrent433',
        accountType: 'Current',
        balanceDate: '08/11/2018',
        currency: 'AUD',
        openingAvailableBalance: 38010.62
    },
    {
        id: 4,
        accountNumber: 347786244,
        accountName: 'SGCurrent166',
        accountType: 'Current',
        balanceDate: '08/11/2018',
        currency: 'SGD',
        openingAvailableBalance: 50664.65
    },
    {
        id: 5,
        accountNumber: 680168913,
        accountName: 'AUCurrent374',
        accountType: 'Current',
        balanceDate: '08/11/2018',
        currency: 'AUD',
        openingAvailableBalance: 41327.28
    },
    {
        id: 6,
        accountNumber: 136056165,
        accountName: 'AUSavings938',
        accountType: 'Savings',
        balanceDate: '08/11/2018',
        currency: 'AUD',
        openingAvailableBalance: 48928.79
    },
    {
        id: 7,
        accountNumber: 453963528,
        accountName: 'SGSavings842',
        accountType: 'Savings',
        balanceDate: '08/11/2018',
        currency: 'SGD',
        openingAvailableBalance: 72117.53
    },
    {
        id: 8,
        accountNumber: 334666982,
        accountName: 'AUSavings253',
        accountType: 'Savings',
        balanceDate: '08/11/2018',
        currency: 'AUD',
        openingAvailableBalance: 20588.16
    },
    {
        id: 9,
        accountNumber: 793949180,
        accountName: 'AUCurrent754',
        accountType: 'Current',
        balanceDate: '08/11/2018',
        currency: 'AUD',
        openingAvailableBalance: 88794.48
    },
    {
        id: 10,
        accountNumber: 768759901,
        accountName: 'SGCurrent294',
        accountType: 'Current',
        balanceDate: '08/11/2018',
        currency: 'SGD',
        openingAvailableBalance: 5906.55
    },
    {
        id: 11,
        accountNumber: 847257972,
        accountName: 'AUCurrent591',
        accountType: 'Current',
        balanceDate: '01/01/2019',
        currency: 'AUD',
        openingAvailableBalance: 92561.68
    }
]