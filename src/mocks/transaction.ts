import { Transaction } from 'types/transaction'

export interface TransactionList {
    [accountID: number]: Transaction[]
}

export const transactions: TransactionList = {
    1: [
        {
            id: 11111,
            valueDate: '01/12/2012', // to keep UI consistency easily, mock this date format same as accounts balancer date format
            creditAmount: 9540.98,
            debitOrCredit: 'Credit',
            transactionNarrative: 'food'
        },
        {
            id: 2222,
            valueDate: '02/12/2012',
            debitAmount: 7497.82,
            debitOrCredit: 'Debit',
        },
        {
            id: 3333,
            valueDate: '03/12/2012',
            creditAmount: 5564.79,
            debitOrCredit: 'Credit',
        }
    ],
    2: [
        {
            id: 4444,
            valueDate: '04/12/2012',
            creditAmount: 8136.18,
            debitOrCredit: 'Credit',
        },
        {
            id: 5555,
            valueDate: '05/12/2012',
            debitAmount: 9442.46,
            debitOrCredit: 'Debit',
        },
        {
            id: 6666,
            valueDate: '01/11/2012',
            creditAmount: 7614.45,
            debitOrCredit: 'Credit',
        }
    ],
    3: [
        {
            id: 7777,
            valueDate: '01/10/2012',
            debitAmount: 3311.65,
            debitOrCredit: 'Debit',
        }
    ],
    4: [
        {
            id: 8888,
            valueDate: '02/22/2012',
            creditAmount: 9198.09,
            debitOrCredit: 'Credit',
        },
        {
            id: 9999,
            valueDate: '03/12/2012',
            creditAmount: 1905.86,
            debitOrCredit: 'Credit',
        },
        {
            id: 1010210321,
            valueDate: '04/12/2012',
            debitAmount: 197.78,
            debitOrCredit: 'Debit',
        },        {
            id: 123131213,
            valueDate: '05/12/2012',
            creditAmount: 8430.49,
            debitOrCredit: 'Credit',
        }
        ,        {
            id: 2342432432,
            valueDate: '06/12/2012',
            debitAmount: 8.33,
            debitOrCredit: 'Debit',
        }
        ,        {
            id: 343535,
            valueDate: '07/12/2012',
            creditAmount: 132.98,
            debitOrCredit: 'Credit',
        }
    ],
    5: [
        {
            id: 45464564,
            valueDate: '08/12/2012',
            creditAmount: 3233.23,
            debitOrCredit: 'Credit',
        },
        {
            id: 5675756,
            valueDate: '09/16/2012',
            creditAmount: 4232.15,
            debitOrCredit: 'Credit',
        }
    ],
    6: [
        {
            id: 678687,
            valueDate: '01/14/2012',
            debitAmount: 7878.67,
            debitOrCredit: 'Debit',
        },
        {
            id: 7897987,
            valueDate: '02/12/2012',
            creditAmount: 6767.67,
            debitOrCredit: 'Credit',
        },
        {
            id: 89080980,
            valueDate: '01/12/2012',
            debitAmount: 57.98,
            debitOrCredit: 'Debit',
        }
    ],
    7: [
        {
            id: 997689768,
            valueDate: '01/17/2012',
            creditAmount: 585.33,
            debitOrCredit: 'Credit',
        },
        {
            id: 134141415,
            valueDate: '01/19/2012',
            debitAmount: 464.56,
            debitOrCredit: 'Debit',
        }
    ],
    8: [
        {
            id: 252626262,
            valueDate: '01/22/2012',
            creditAmount: 5656.56,
            debitOrCredit: 'Credit',
        }
    ],
    9: [
        {
            id: 37338383,
            valueDate: '01/13/2012',
            creditAmount: 9999.99,
            debitOrCredit: 'Credit',
        },
        {
            id: 484894848,
            valueDate: '01/13/2012',
            creditAmount: 1.98,
            debitOrCredit: 'Credit',
        },
        {
            id: 5959577,
            valueDate: '01/19/2012',
            debitAmount: 2.34,
            debitOrCredit: 'Debit',
        }
    ],
    10: [
        {
            id: 615512,
            valueDate: '01/10/2012',
            creditAmount: 4334.23,
            debitOrCredit: 'Credit',
        }
    ],
    11: [
        {
            id: 632632,
            valueDate: '01/12/2012',
            debitAmount: 5642.78,
            debitOrCredit: 'Debit',
        }
    ]
}