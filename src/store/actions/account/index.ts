import { actions as ActionTypes } from 'constants/action'
import { FetchAccountsAction } from 'types/account'

// since there is no backend, there will be no redux middleware here
export const fetchAccountsAction: (paccount: string) => FetchAccountsAction = (paccount) => {
    return {
        type: ActionTypes.FETCH_ACCOUNTS,
        paccount,
    }
}