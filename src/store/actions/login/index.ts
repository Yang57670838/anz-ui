import { actions as ActionTypes } from 'constants/action'
import { LoginAction, LogoutAction } from 'types/login'

// since there is no backend, there will be no redux middleware here
export const loginAction: (account: string, password: string, rememberMe: boolean) => LoginAction = (account, password, rememberMe) => {
    return {
        type: ActionTypes.LOGIN,
        account,
        password,
        rememberMe
    }
}

export const logoutAction: () => LogoutAction = () => {
    return {
        type: ActionTypes.LOGOUT,
    }
}
