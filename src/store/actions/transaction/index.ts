import { actions as ActionTypes } from 'constants/action'
import { FetchTransactionsAction } from 'types/transaction'

// since there is no backend, there will be no redux middleware here
export const fetchTransactionsAction: (accountID: number) => FetchTransactionsAction = (accountID) => {
    return {
        type: ActionTypes.FETCH_TRANSACTIONS,
        accountID,
    }
}