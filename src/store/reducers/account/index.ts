import { Reducer } from 'redux'
// import { mapKeys } from 'lodash'
import { actions as ActionTypes } from 'constants/action'
import { AccountStore, AccountActions, AccountDetail } from 'types/account'
import { accountList } from 'mocks/account'

// normalize the array of accounts into redux store
let normalizedAccounts: {
  [accountID: number]: AccountDetail
}
import(/* webpackChunkName: "Lodash" */ 'lodash').then(({ default: _ }) => {
  normalizedAccounts = _.mapKeys(accountList, 'id' )
}).catch(error => 'An error occurred while loading lodash');

const initialState: AccountStore = {
  accounts: null
};

export const accountReducer: Reducer<AccountStore, AccountActions> = (state: AccountStore = initialState, action: AccountActions): AccountStore => {
  switch (action.type) {
    case ActionTypes.FETCH_ACCOUNTS:
      return {
        accounts: normalizedAccounts
      };
    default:
      return state;
  }
}

