import { loginReducer } from './login'
import { accountReducer } from './account'
import { transactionReducer } from './transaction'
import { combineReducers } from 'redux'
import { actions as ActionTypes } from 'constants/action'
import { Store } from 'types/common/store'

const appReducer = combineReducers({
    login: loginReducer,
    account: accountReducer,
    transaction: transactionReducer
})

const rootReducer = (state: Store, action: any) => {
    if (action.type === ActionTypes.LOGOUT) {
        //  persist store will also then save the init state of login reducer, which is empty data..
        return appReducer(undefined, action);
    }
    return appReducer(state, action);
};


export default rootReducer

export type AppState = ReturnType<typeof rootReducer>

