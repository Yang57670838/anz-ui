import jwt_decode from 'jwt-decode'
import { LoginStore } from 'types/login'
import { JWTmock, loginMock } from 'mocks/account'

function loginHandler(state: LoginStore, account: string, password: string, rememberMe: boolean): LoginStore {

    // mock the auth
    if (loginMock.paccount === account && loginMock.password === password) {
        const currentTime = Math.round(Date.now() / 1000)
        const decodedPayload: any = jwt_decode(JWTmock.value)
        if (rememberMe) {
            localStorage.setItem('paccount', account);
        } else {
            localStorage.removeItem('paccount');
        }
        return {
            name: decodedPayload.name,
            exp: currentTime+3600, // mock to expire after one hour
            token: JWTmock.value,
            paccount: decodedPayload.paccount,
            error: false
        }
    }
    return {
        ...state,
        error: true
    }
}

export default loginHandler
