import { Reducer } from 'redux'
import { actions as ActionTypes } from 'constants/action'
import { LoginStore, LoginActions } from 'types/login'
import loginHandler from './handler/loginHandler'

const initialState: LoginStore = {
  name: null,
  exp: null,
  token: null,
  paccount: null,
  error: false
};

export const loginReducer: Reducer<LoginStore, LoginActions> = (state: LoginStore = initialState, action: LoginActions): LoginStore => {
  switch (action.type) {
    case ActionTypes.LOGIN:
      return loginHandler(state, action.account, action.password, action.rememberMe);
    default:
      return state;
  }
}