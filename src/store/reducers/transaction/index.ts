import { Reducer } from 'redux'
import { actions as ActionTypes } from 'constants/action'
import { TransactionStore, TransactionActions } from 'types/transaction'
import { transactions } from 'mocks/transaction'

const initialState: TransactionStore = {
    transactions: []
};

export const transactionReducer: Reducer<TransactionStore, TransactionActions> = (state: TransactionStore = initialState, action: TransactionActions): TransactionStore => {
  switch (action.type) {
    case ActionTypes.FETCH_TRANSACTIONS:
      return {
        transactions: transactions[action.accountID]
      };
    default:
      return state;
  }
}