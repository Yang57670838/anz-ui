import { createMuiTheme } from '@material-ui/core/styles';
import { IThemeOptions } from 'types/common/theme'

const ANZPrimaryBlue: string = '#0E4165'
const ANZSecondaryBlue: string = '#DFC5A9'

export default createMuiTheme({
    palette: {
        primary: {
          main: `${ANZPrimaryBlue}`,
        },
        secondary: {
          main: `${ANZSecondaryBlue}`,
        },
      },
    typography: {
        tab: {
            textTransform: 'none',
            fontweight: 700,
            fontSize: '1rem',
        },
        headerButton: {
            fontSize: '1rem',
            textTransform: 'none',
            color: 'white'
        }
    }
  } as IThemeOptions)