import { actions as ActionTypes } from 'constants/action'

export interface AccountDetail {
    id: number;
    accountNumber: number;
    accountName: string;
    accountType: 'Savings' | 'Current';
    balanceDate: string;
    currency: 'SGD' | 'AUD';
    openingAvailableBalance: number;
}

export interface FetchAccountsAction {
    type: ActionTypes.FETCH_ACCOUNTS;
    paccount: string;
}

export type AccountActions =
| FetchAccountsAction;

export interface AccountStore {
    accounts: {
        [accountID: number]: AccountDetail
    } | null
}


