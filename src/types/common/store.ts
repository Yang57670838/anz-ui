import { LoginStore } from '../login'
import { AccountStore } from '../account'
import { TransactionStore } from '../transaction'

export interface Store {
    login: LoginStore;
    account: AccountStore;
    transaction: TransactionStore;
}