import { actions as ActionTypes } from 'constants/action'

export interface LoginAction {
    type: ActionTypes.LOGIN;
    account: string;
    password: string;
    rememberMe: boolean;
}

export interface LogoutAction {
    type: ActionTypes.LOGOUT;
}

export type LoginActions =
| LoginAction
| LogoutAction;

export interface LoginStore {
    name: string | null;
    paccount: string | null;
    exp: number | null;
    token: string | null;
    error: boolean;
}


