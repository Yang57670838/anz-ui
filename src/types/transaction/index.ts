import { actions as ActionTypes } from 'constants/action'

export interface Transaction {
    id: number;
    valueDate: string;
    debitAmount?: number;
    creditAmount?: number;
    debitOrCredit: 'Credit' | 'Debit';
    transactionNarrative?: string;
}

export interface FetchTransactionsAction {
    type: ActionTypes.FETCH_TRANSACTIONS;
    accountID: number;
}

export type TransactionActions =
| FetchTransactionsAction;

export interface TransactionStore {
    transactions: Transaction[];
}