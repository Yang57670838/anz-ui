import { isAccountNumber } from './validate';

describe('Validation functions', () => {

    const mockAccount1 = ''
    const mockAccount2 = '123456789'
    const mockAccount3 = 'dsadwqew'
    const mockAccount4 = '1234567890'

    it('isAccountNumber function is working correctly', () => {
        expect(isAccountNumber(mockAccount1)).toBeFalsy()
        expect(isAccountNumber(mockAccount2)).toBeTruthy()
        expect(isAccountNumber(mockAccount3)).toBeFalsy()
        expect(isAccountNumber(mockAccount4)).toBeFalsy()
    });
});