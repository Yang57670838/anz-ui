// assume the login primary account can be 1-9 digits, then identify server will auth
export const isAccountNumber = (value: string): boolean => {
    return /^\d{1,9}$/.test(value);
}